export class UsuarioDTO {
  nome: string;
  email: string;
  senha: string;
  papel: string;

  constructor(nome, email, senha, papel){
    this.nome = nome;
    this.email = email;
    this.senha = senha;
    this.papel = papel;
  }
}
