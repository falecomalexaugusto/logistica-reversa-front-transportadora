export class BarcodeConfig {

    elementType: string;
    format: string;
    lineColor: string;
    width: number;
    height: number;
    displayValue: boolean;
    fontOptions: string;
    font: string;
    textAlign: string;
    textPosition: string;
    textMargin: number;
    fontSize: number;
    background: string;
    margin: number;
    marginTop: number;
    marginBottom: number;
    marginLeft: number;
    marginRight: number;
    printCSS: string[];
    printStyle: string;

  constructor() {
    this.format = 'CODE39';
    this.elementType = 'svg';
    this.lineColor = '#000000';
    this.width = 1;
    this.height = 50;
    this.displayValue = true;
    this.fontOptions = '';
    this.font = 'monospace';
    this.textAlign = 'center';
    this.textPosition = 'bottom';
    this.textMargin = 2;
    this.fontSize = 15;
    this.background = '#ffffff';
    this.margin = 0;
    this.marginTop = 0;
    this.marginBottom = 0;
    this.marginLeft = 0;
    this.marginRight = 0;

    this.printCSS = ['http://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css'];

    this.printStyle =
      `
        .column {
          float: left;
        }

        .column > h4 {
          transform: rotate(-90deg);
        }

        .row:after {
          content: "";
          display: table;
          clear: both;
        }

      `;
  }
  
}