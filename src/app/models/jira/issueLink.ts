export class IssueLink {
  id: string;
  type: string;
  outWardIssue: string[];
  inWardIssue: string[];

  constructor(){}
}
