export enum IssueType {
  SUPPLIES = 'Supplies',
  HARDWARE_LEX = 'Hardware Lex',
  REMANEJAMENTO = 'Remanejamento',
  ECOBIN_PLANEJADA = 'Ecobin Planejada',
  ECOBIN_EMERGENCIAL = 'Ecobin Emergencial',
  HARDWARE = 'Hardware',
  HARDWARE_COLETA = 'Hardware Coleta'
}
