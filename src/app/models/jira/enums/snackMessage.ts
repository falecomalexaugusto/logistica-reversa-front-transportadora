export enum SnackMessage {
    
    TAREFA_ENCONTRADA = 'Tarefa Encontrada.',
    TAREFA_ATUALIZADA = 'Tarefa Atualizada.',
    TAREFAS_LINCADAS = 'PROCESSO ENCERRADO: Tarefas lincadas e atualizadas no JIRA.',
    CRIACAO_TAREFA = 'Será criado uma nova tarefa',
    
    TAREFA_NAO_POSSUI_LINKS = 'Tarefa sem outras tarefas lincadas.',
    
    TAREFA_NAO_ENCONTRADA = 'Tarefa não Encontrada.',
    TAREFAS_NAO_ENCONTRADAS = 'Tarefas não encontradas.',
    PROBLEMA_ATUALIZAR_TAREFA = 'Não foi possível em Atualizar a Tarefa.',

    VALIDE_TENTE_NOVAMENTE = 'Valide esta Informação e tente novamente.',
    
    IMPRIMIR_ETIQUETA = 'Agora é possivel imprimir a Etiqueta.',

    VALOR_CUSTOM_FIELD_NAO_ENCONTRADO = 'O valor do Custom Field não foi Encontrado.',

    AGUARDE = 'Aguarde o processo ser finalizado..',
    PROCESSO_FINALIZADO = 'Processo finalizado.',

    PROBLEMA_CRIACAO_TAREFA = 'Não foi possível em criar a tarefa.',
    PROBLEMA_LINK_TAREFA = 'Problema ao lincar as tarefas no JIRA.',
    PROBLEMA_ENVIAR_EMAIL = 'Problema em enviar o email aos destinatarios.',

}