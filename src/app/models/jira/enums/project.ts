export enum Project {
  TBLEX = 'TBLEX',
  TB = 'TB',
  TBSPR = 'TBSPR',
  TBSIN = 'TBSIN',
  TBHPE = 'TBHPE',
  TBOKI = 'TBOKI',
}
