import { Project } from './enums/project';
import { IssueLink } from './issueLink';
import { IssueType } from './enums/issue_type';
import { isObject } from 'util';

export class Issue {

  issueType: string;
  project: string;
  issueKey: string;

  constructor() { }

}
