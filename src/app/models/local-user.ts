export interface LocalUser {
  nome: string;
  email: string;
  papel: string;
}
