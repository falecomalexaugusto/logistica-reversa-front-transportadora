import { ATLASSIAN_CONFIG } from './../../../config/api.config';
import { CredencialDTO } from '../../models/credencialDTO';
import { API_CONFIG } from '../../../config/api.config';
import { UsuarioDTO } from '../../models/usuarioDTO';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LocalUser } from 'src/app/models/local-user';
import { StorageService } from '../storage/storage.service';
const httpOptions = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  }),
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(public httpClient: HttpClient,public storage: StorageService) { }

  cadastrarUsuario(usuarioDTO: UsuarioDTO) {
    console.log(usuarioDTO)
    return this.httpClient.post(`${API_CONFIG.SERVER_BASE_URL}/users`, JSON.stringify(usuarioDTO), httpOptions);
  }

  login(credencialDTO: CredencialDTO): Observable<any> {
    return this.httpClient.post(`${API_CONFIG.SERVER_BASE_URL}/login`, JSON.stringify(credencialDTO), httpOptions);
  }

  successfullLogin(nome: string, email: string, papel: string) {
    let user: LocalUser = {
      nome: nome,
      email: email,
      papel: papel
    };
    this.storage.setLocalUser(user);
  }

  logout() {
    this.storage.setLocalUser(null);
  }

}
