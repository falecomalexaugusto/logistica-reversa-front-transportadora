import { TestBed } from '@angular/core/testing';

import { JiraCloudService } from './jira-cloud.service';

describe('JiraCloudService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JiraCloudService = TestBed.get(JiraCloudService);
    expect(service).toBeTruthy();
  });
});
