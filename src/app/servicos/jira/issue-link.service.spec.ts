import { TestBed } from '@angular/core/testing';

import { IssueLinkService } from './issue-link.service';

describe('IssueLinkService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IssueLinkService = TestBed.get(IssueLinkService);
    expect(service).toBeTruthy();
  });
});
