import { Injectable } from '@angular/core';
import { API_CONFIG } from 'src/config/api.config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Origin': '*',
    'Content-Type':  'application/json'
  }),
};
@Injectable({
  providedIn: 'root'
})
export class IssueLinkService {

  constructor(private httpClient: HttpClient) { }

  create(json: string) {

    return this.httpClient.post(`${API_CONFIG.SERVER_JIRA_URL}/issue-link`, json, httpOptions);
  }

  delete(issueKey: string) {
    return this.httpClient.delete(`${API_CONFIG.SERVER_JIRA_URL}/issue-link/`.concat(issueKey));
  }

  find(issueKey: string): Observable<any> {
    return this.httpClient.get(`${API_CONFIG.SERVER_JIRA_URL}/issue-link/`.concat(issueKey), httpOptions);
  }

}
