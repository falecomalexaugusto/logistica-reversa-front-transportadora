import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API_CONFIG } from 'src/config/api.config';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Origin': '*',
    'Content-Type':  'application/json'
  }),
};
@Injectable({
  providedIn: 'root'
})
export class IssueService {

  constructor(private httpClient: HttpClient) { }

  create(json: string): Observable<any> {
    return this.httpClient.post(`${API_CONFIG.SERVER_JIRA_URL}/issue/`, json, httpOptions);
  }

  delete(issueKey: string) {
    return this.httpClient.delete(`${API_CONFIG.SERVER_JIRA_URL}/issue/`.concat(issueKey));
  }

  findByKey(issueKey: string): Observable<any> {
    return this.httpClient.get(`${API_CONFIG.SERVER_JIRA_URL}/issue/`.concat(issueKey), { observe: 'response' });
  }

  findByJQL(json: string): Observable<any> {
    return this.httpClient.post(`${API_CONFIG.SERVER_JIRA_URL}/jql/search`, json, httpOptions);
  }

  update(issueKey: string, json: string): Observable<any> {
    return this.httpClient.put(`${API_CONFIG.SERVER_JIRA_URL}/issue/`.concat(issueKey), json, httpOptions);
  }

  updateComment(issueKey: string, json: string){
    return this.httpClient.post(`${API_CONFIG.SERVER_JIRA_URL}/issue/`.concat(issueKey), json, httpOptions);
  }

  transition(issueKey: string, json: string): Observable<any> {
    return this.httpClient.post(`${API_CONFIG.SERVER_JIRA_URL}/issueTransition/`.concat(issueKey), json, httpOptions);
  }
}
