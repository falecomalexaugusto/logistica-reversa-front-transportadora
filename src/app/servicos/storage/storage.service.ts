import { Injectable } from '@angular/core';
import { LocalUser } from 'src/app/models/local-user';
import { STORAGE_KEYS } from 'src/config/storage-keys';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  getLocalUser() : LocalUser{
    let user = localStorage.getItem(STORAGE_KEYS.email);
    if(user == null){
        return null;
    }else{
        return JSON.parse(user);
    }
}

setLocalUser(obj: LocalUser){
    if(obj == null){
        localStorage.removeItem(STORAGE_KEYS.email);
    }else{
        localStorage.setItem(STORAGE_KEYS.email, JSON.stringify(obj));
    }
}

}
