import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_CONFIG } from 'src/config/api.config';

@Injectable({
  providedIn: 'root'
})
export class JiraCloudService {

  constructor(private httpClient: HttpClient) {

   }

   buscaPropriedadesDaIssue(key: string): Observable<any> {
     return this.httpClient.get(`${API_CONFIG.SERVER_JIRA_URL}/issue/`.concat(key));
   }

   updateIssue(key: string, issue: string) {
     return this.httpClient.put(`${API_CONFIG.SERVER_JIRA_URL}/issue/`.concat(key), issue);
   }

   buscarIssuePeloJQL(jql: string) {
     return this.httpClient.get(`${API_CONFIG.SERVER_JIRA_URL}/search/`.concat(jql));
   }

   criarIssue(issueTypeID: string){
    return this.httpClient.get(`${API_CONFIG.SERVER_JIRA_URL}/issue/create/`.concat(issueTypeID));
   }

   criarLinkEntreDuasIssues(issueA: string, issueB: string){
    return this.httpClient.get(`${API_CONFIG.SERVER_JIRA_URL}/issue/createLink/`.concat(issueA).concat('/').concat(issueB));
   }
}
