import { IssueLinkService } from './../servicos/jira/issue-link.service';
import { IssueService } from './../servicos/jira/issue.service';
import { Issue } from '../models/jira/issue';
import { IssueLink } from '../models/jira/issueLink';


//USAR SOMENTE ESSA CLASSE FUTURAMENTE
export class JiraComponent {

  issues: Issue[];
  issueLinks: IssueLink[];

  constructor(private issueService: IssueService,
              private issueLinkService: IssueLinkService) { }

  // createIssue(issue: Issue) {
  //   this.issueService.create(issue).subscribe(result => {
  //     //CRIAR OBJETO ISSUE A PARTIR DO RESULT E ARMAZENAR NO VETOR DE ISSUES
  //     console.log(result);
  //     return 200;
  //   }, error => {
  //     console.log('Nao foi possivel criar a Tarefa, segue o erro: ');
  //     console.log(error);
  //     return 400;
  //   });
  // }

  deleteIssue(issueKey: string) {
    this.issueService.delete(issueKey).subscribe(result => {
      console.log(result);
      return 200;

    }, error => {
      console.log('Nao foi possivel deletar a Tarefa, segue o erro: ');
      console.log(error);
      return 400;

    });
  }

  find(issueKey: string) {
    this.issueService.findByKey(issueKey).subscribe(result => {
      //CRIAR OBJETO ISSUE A PARTIR DO RESULT E ARMAZENAR NO VETOR DE ISSUES
      console.log(result);
      return 200;
    }, error => {
      console.log('Nao foi possivel buscar a tarefa: '.concat(issueKey).concat(', segue o erro:'));
      console.log(error);
      return 400;

    });
  }

  findByJQL(jql: string) {
    this.issueService.findByJQL(jql).subscribe(result => {
      //CRIAR OBJETO ISSUE A PARTIR DO RESULT E ARMAZENAR NO VETOR DE ISSUES
      console.log(result);
      return 200;
    }, error => {
      console.log('Nao foi possivel buscar a tarefa, segue o erro:');
      console.log(error);
      return 400;
    });
  }

  updateIssue(issueKey: string, json: string) {
    this.issueService.update(issueKey, json).subscribe(result => {
      console.log(result);
      return 200;

    }, error => {
      console.log('Nao foi possivel atualizar a tarefa, segue o erro:');
      console.log(error);
      return 400;
    });
  }

  // createLink(issueLink: IssueLink){
  //   this.issueLinkService.create(issueLink).subscribe(result => {
  //     console.log(result);
  //     return 200;

  //   }, error => {
  //     console.log('Nao foi possivel criar o Link, segue o erro: ');
  //     console.log(error);
  //     return 400;
  //   });
  // }

  deleteLink(issueLinkId: string){
    this.issueLinkService.delete(issueLinkId).subscribe(result => {
      console.log(result);
      return 200;
    }, error => {
      console.log('Nao foi possivel Deletar o Link, segue o erro: ');
      console.log(error);
      return 400;
    });
  }

  findLink(issueLinkId: string){
    this.issueLinkService.find(issueLinkId).subscribe(result => {
      console.log(result);
      return 200;
    }, error => {
      console.log('Nao foi possivel Buscar o Link, segue o erro: ');
      console.log(error);
      return 400;
    });
  }
}
