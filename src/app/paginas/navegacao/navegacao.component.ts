import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/servicos/storage/storage.service';
import { AuthService } from 'src/app/servicos/auth/auth.service';
import { Router } from '@angular/router';
import { LocalUser } from 'src/app/models/local-user';

@Component({
  selector: 'app-navegacao',
  templateUrl: './navegacao.component.html',
  styleUrls: ['./navegacao.component.scss']
})
export class NavegacaoComponent implements OnInit {

  constructor(private router: Router,
    private storage: StorageService) {
    
  }

  ngOnInit() {
    /*
    if (this.storage.getLocalUser() == null) {
      alert('Voce precisa logar no sistema para acessar esses modulos.');
      this.router.navigate(['/']);
    }
    */
  }


  sendTriagemHPScreen() {
    this.router.navigate(['/triagem-hp'])
  }

  sendTriagemLexmarkScreen() {
    this.router.navigate(['/triagem-lexmark'])
  }

  sendTriagemOkidataScreen() {
    this.router.navigate(['/triagem-okidata'])
  }

  sendTriagemSimpresScreen() {
    this.router.navigate(['/triagem-simpress'])
  }

}
