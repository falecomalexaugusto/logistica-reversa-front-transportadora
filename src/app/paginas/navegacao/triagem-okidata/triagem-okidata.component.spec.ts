import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TriagemOkidataComponent } from './triagem-okidata.component';

describe('TriagemOkidataComponent', () => {
  let component: TriagemOkidataComponent;
  let fixture: ComponentFixture<TriagemOkidataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TriagemOkidataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TriagemOkidataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
