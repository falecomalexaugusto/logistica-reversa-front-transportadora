import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Issue } from 'src/app/models/jira/issue';
import { FormControl } from '@angular/forms';
import { IssueService } from 'src/app/servicos/jira/issue.service';
import { REGEX_PATTERN, LONG } from 'src/app/utils/const/regex.const';
import * as jsonFactory from '../../../utils/json';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-triagem-okidata',
  templateUrl: './triagem-okidata.component.html',
  styleUrls: ['./triagem-okidata.component.scss']
})
export class TriagemOkidataComponent implements OnInit {

  issue: Issue
  preAlertaId = new FormControl({value: '', disabled: true})
  palletId = new FormControl({value: '', disabled: true})
  qntInformadaCliente = new FormControl({value: '', disabled: true})
  serialNumbers: Array<string> = new Array
  customfield_13001_OKI_TONER: any
  customfield_13002_NAO_OKI_TONER: any
  serialNumberForm = new FormControl({value: '', disabled: true})

  constructor(private router: Router, private issueService: IssueService,
    public snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  back() {
    this.router.navigate(['/sistema'])
  }

  paste(event: any){
    var value = event.clipboardData.getData('text');
    if(value != '' && REGEX_PATTERN.JIRA_KEY.test(value)){
      this.issueService.findByKey(value).subscribe(result => {
        if(result.body.errorMessages === undefined){
          var objectIssueResult = result.body.SuccessData
          console.log(objectIssueResult)
          this.issue = {
            issueKey: objectIssueResult.key,
            issueType: objectIssueResult.fields.issuetype.name,
            project: objectIssueResult.project
          }
          this.preAlertaId.setValue(objectIssueResult.fields.customfield_12501 == null ? '' : objectIssueResult.fields.customfield_12501)
          this.palletId.setValue(objectIssueResult.fields.customfield_12502  == null ? '' : objectIssueResult.fields.customfield_12502)  
          this.qntInformadaCliente.setValue(objectIssueResult.fields.customfield_16664  == null ? '' : objectIssueResult.fields.customfield_16664)
          this.customfield_13001_OKI_TONER = objectIssueResult.fields.customfield_13001 == null ? 0 : objectIssueResult.fields.customfield_13001
          this.customfield_13002_NAO_OKI_TONER = objectIssueResult.fields.customfield_13002 == null ? 0 : objectIssueResult.fields.customfield_13002

          this.serialNumberForm.enable()
        } else {
          this.limpaInformacoesTB()
        }
      }, 
        error => console.log(error)
      )
    } else {
      this.limpaInformacoesTB()
    }
  }
    
  private updatePalletAndPrealertaField(objectIssueResult: any){
    for(var i = 0;i<objectIssueResult.fields.issuelinks.length;i++) {
      if(objectIssueResult.fields.issuelinks[i].outwardIssue === undefined){
        return;
      }
      var outwardIssue = objectIssueResult.fields.issuelinks[i].outwardIssue.key
      if(outwardIssue.startsWith('PALLET')){
        this.palletId.setValue(outwardIssue.concat('\n').concat(this.palletId.value))
      }
      if(outwardIssue.startsWith('PREALERTA')){
        this.preAlertaId.setValue(outwardIssue.concat('\n').concat(this.palletId.value))
      }
    }
  }

  private limpaInformacoesTB(){
    this.preAlertaId.setValue('SEM VALOR')
    this.palletId.setValue('SEM VALOR')
    this.qntInformadaCliente.setValue('SEM VALOR')
  }

  adicionarSerialNumber(event: any) {
    this.serialNumbers.push(event.clipboardData.getData('text').trim())
    this.serialNumberForm.setValue('')
  }

  removeSerialNumber(serialNumber: string) {
    const index: number = this.serialNumbers.indexOf(serialNumber);
    if (index !== -1) {
      this.serialNumbers.splice(index, 1);
    }   
  }

  okidataSemCodigoPlus() {
    this.customfield_13001_OKI_TONER++
  }

  okidataSemCodigoMinus() {
    this.customfield_13001_OKI_TONER--
  }

  naoOkidataPlus() {
    this.customfield_13002_NAO_OKI_TONER++
  }

  naoOkidataMinus() {
    this.customfield_13002_NAO_OKI_TONER--
  }

  triar() {
    if(this.issue != undefined){
      var snackBarRef = this.snackBar.open('Processo de Triagem iniciado. O Jira esta sendo atualizado. Aguarde...'
      , 'Fechar' , { duration: 3000});

      this.updateIssue();
      this.limpaInformacoesTB()
      this.transitionIssue();

      snackBarRef.afterDismissed().subscribe(() => {
        this.snackBar.open('Processo de Triagem Finalizado...', 'Fechar' , { duration: 5000});
      })
    }
  }

  private transitionIssue() {
    var jsonTransition = jsonFactory.createJsonForTransactionIssue('331').getJson()
    this.issueService.transition(this.issue.issueKey, jsonTransition).subscribe(result => {
      console.log(result)
      }, error => console.log(error)
    )
  }
  
  private updateIssue() {
    var jsonOkiToner = jsonFactory.createJsonForUpdateFieldsNumberType("customfield_13001", this.customfield_13001_OKI_TONER).getJson();
    var jsonNaoOkiToner = jsonFactory.createJsonForUpdateFieldsNumberType("customfield_13002", this.customfield_13002_NAO_OKI_TONER).getJson();
    var jsonUpdateSerialNumber = jsonFactory.createJsonForUpdateTextAreaDocumentType("customfield_16594", this.serialNumbers).getJson();

    let totalTonersTriados = this.customfield_13001_OKI_TONER + this.customfield_13002_NAO_OKI_TONER + this.serialNumbers.length
    var jsonTotalTonersTriados = jsonFactory.createJsonForUpdateFieldsNumberType("customfield_15005", totalTonersTriados).getJson();

    this.issueService.update(this.issue.issueKey, jsonOkiToner).subscribe(result => {
      console.log(result);
    }, error => console.log(error));

    this.issueService.update(this.issue.issueKey, jsonNaoOkiToner).subscribe(result => {
      console.log(result);
    }, error => console.log(error));

    this.issueService.update(this.issue.issueKey, jsonUpdateSerialNumber).subscribe(result => {
      console.log(result)
      }, error => console.log(error)
    )

    this.issueService.update(this.issue.issueKey, jsonTotalTonersTriados).subscribe(result => {
      console.log(result)
      }, error => console.log(error)
    )

    this.limpaInformacoesTB()
  }

  isIssueExists(): boolean {
    return this.issue === undefined
  }  
}
