import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Issue } from 'src/app/models/jira/issue';
import { FormControl } from '@angular/forms';
import { IssueService } from 'src/app/servicos/jira/issue.service';
import { REGEX_PATTERN, LONG } from 'src/app/utils/const/regex.const';
import * as jsonFactory from '../../../utils/json';

@Component({
  selector: 'app-triagem-simpress',
  templateUrl: './triagem-simpress.component.html',
  styleUrls: ['./triagem-simpress.component.scss']
})
export class TriagemSimpressComponent {
  
  issue: Issue
  preAlertaId = new FormControl({value: '', disabled: true})
  palletId = new FormControl({value: '', disabled: true})
  qntInformadaCliente = new FormControl({value: '', disabled: true})
  
  serialNumbers: Array<string> = new Array
  serialNumberForm = new FormControl({value: '', disabled: true})

  customfield_16633_TONER_NOVOS: any
  customfield_16634_TONER_TESTE: any
  customfield_16635_TONER_DESTRUICAO: any
  customfield_16654_DIVERSOS: any
  snackBar: any;

  constructor(private router: Router, private issueService: IssueService) { }

  back() {
    this.router.navigate(['/sistema'])
  }

  paste(event: any){
    var value = event.clipboardData.getData('text').trim();
    if(value != '' && REGEX_PATTERN.JIRA_KEY.test(value)){
      this.issueService.findByKey(value).subscribe(result => {
        if(result.body.errorMessages === undefined){
          var objectIssueResult = result.body.SuccessData
          console.log(objectIssueResult)
          this.issue = {
            issueKey: objectIssueResult.key,
            issueType: objectIssueResult.fields.issuetype.name,
            project: objectIssueResult.project
          }
          this.preAlertaId.setValue(objectIssueResult.fields.customfield_12501 == null ? '' : objectIssueResult.fields.customfield_12501)
          this.palletId.setValue(objectIssueResult.fields.customfield_12502  == null ? '' : objectIssueResult.fields.customfield_12502) 
          this.qntInformadaCliente.setValue(objectIssueResult.fields.customfield_16664  == null ? '' : objectIssueResult.fields.customfield_16664)
          this.customfield_16633_TONER_NOVOS = objectIssueResult.fields.customfield_16633 == null ? 0 : objectIssueResult.fields.customfield_16633
          this.customfield_16634_TONER_TESTE = objectIssueResult.fields.customfield_16634 == null ? 0 : objectIssueResult.fields.customfield_16634
          this.customfield_16635_TONER_DESTRUICAO = objectIssueResult.fields.customfield_16635 == null ? 0 : objectIssueResult.fields.customfield_16635
          this.customfield_16654_DIVERSOS = objectIssueResult.fields.customfield_xxxx == null ? 0 : objectIssueResult.fields.customfield_16654

          this.serialNumberForm.enable()
        }
      }, 
        error => console.log(error)
      )
    } else {
      this.limpaInformacoesTB()
    }
  }

  private limpaInformacoesTB(){
    this.preAlertaId.setValue('')
    this.palletId.setValue('')
    this.qntInformadaCliente.setValue('')
  }

  adicionarSerialNumber(event: any) {
    this.serialNumbers.push(event.clipboardData.getData('text').trim())
    this.serialNumberForm.setValue('')
  }

  removeSerialNumber(serialNumber: string) {
    const index: number = this.serialNumbers.indexOf(serialNumber);
    if (index !== -1) {
      this.serialNumbers.splice(index, 1);
    }   
  }

  triar() {
    if(this.issue != undefined){
      var snackBarRef = this.snackBar.open('Processo de Triagem iniciado. O Jira esta sendo atualizado. Aguarde...'
      , 'Fechar' , { duration: 3000});

      this.updateIssue();
      this.transitionIssue();

      snackBarRef.afterDismissed().subscribe(() => {
        this.snackBar.open('Processo de Triagem Finalizado...', 'Fechar' , { duration: 5000});
        this.issue = undefined
        this.limpaInformacoesTB()
      })
    }
  }

  private updateIssue() {
    var jsonDiversos = jsonFactory.createJsonForUpdateFieldsNumberType("customfield_12100", this.customfield_16654_DIVERSOS).getJson();
    var jsonTonerNovos = jsonFactory.createJsonForUpdateFieldsNumberType("customfield_12102", this.customfield_16633_TONER_NOVOS).getJson();
    var jsonTonerTeste = jsonFactory.createJsonForUpdateFieldsNumberType("customfield_12101", this.customfield_16634_TONER_TESTE).getJson();
    var jsonTonerDestruicao = jsonFactory.createJsonForUpdateFieldsNumberType("customfield_12103", this.customfield_16635_TONER_DESTRUICAO).getJson();
    var jsonUpdateSerialNumber = jsonFactory.createJsonForUpdateTextAreaDocumentType("customfield_16594", this.serialNumbers).getJson();

    this.issueService.update(this.issue.issueKey, jsonDiversos).subscribe(result => {
      console.log(result);
    }, error => console.log(error));
    
    this.issueService.update(this.issue.issueKey, jsonTonerNovos).subscribe(result => {
      console.log(result);
    }, error => console.log(error));

    this.issueService.update(this.issue.issueKey, jsonTonerTeste).subscribe(result => {
      console.log(result);
    }, error => console.log(error));

    this.issueService.update(this.issue.issueKey, jsonTonerDestruicao).subscribe(result => {
      console.log(result);
    }, error => console.log(error));

    this.issueService.update(this.issue.issueKey, jsonUpdateSerialNumber).subscribe(result => {
      console.log(result)
      }, error => console.log(error)
    )
  }

  isIssueExists(): boolean {
    return this.issue === undefined
  }

  
  private transitionIssue() {
    var jsonTransition = jsonFactory.createJsonForTransactionIssue('781').getJson()
    this.issueService.transition(this.issue.issueKey, jsonTransition).subscribe(result => {
      console.log(result)
      }, error => console.log(error)
    )
  }

}
