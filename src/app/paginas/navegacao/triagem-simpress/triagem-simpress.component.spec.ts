import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TriagemSimpressComponent } from './triagem-simpress.component';

describe('TriagemSimpressComponent', () => {
  let component: TriagemSimpressComponent;
  let fixture: ComponentFixture<TriagemSimpressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TriagemSimpressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TriagemSimpressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
