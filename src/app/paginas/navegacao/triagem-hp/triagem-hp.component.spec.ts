import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TriagemHpComponent } from './triagem-hp.component';

describe('TriagemHpComponent', () => {
  let component: TriagemHpComponent;
  let fixture: ComponentFixture<TriagemHpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TriagemHpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TriagemHpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
