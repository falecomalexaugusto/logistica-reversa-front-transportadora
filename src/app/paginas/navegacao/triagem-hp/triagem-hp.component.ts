import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Issue } from 'src/app/models/jira/issue';
import { FormControl } from '@angular/forms';
import { IssueService } from 'src/app/servicos/jira/issue.service';
import { REGEX_PATTERN } from 'src/app/utils/const/regex.const';
import * as jsonFactory from '../../../utils/json';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-triagem-hp',
  templateUrl: './triagem-hp.component.html',
  styleUrls: ['./triagem-hp.component.scss']
})
export class TriagemHpComponent {

  issue: Issue
  preAlertaId = new FormControl({value: '', disabled: true})
  palletId = new FormControl({value: '', disabled: true})
  qntInformadaCliente = new FormControl({value: '', disabled: true})
  customfield_12100_HP_TONER: any
  customfield_12101_NAO_HP_TONER: any
  customfield_12102_HP_TINTA: any
  customfield_12103_NAO_HP_TINTA: any

  constructor(private router: Router, private issueService: IssueService, public snackBar: MatSnackBar) { }

  back() {
    this.router.navigate(['/sistema'])
  }

  paste(event: any){
    var value = event.clipboardData.getData('text').trim();
    if(value != '' && REGEX_PATTERN.JIRA_KEY.test(value)){
      this.issueService.findByKey(value).subscribe(result => {
        if(result.body.errorMessages === undefined){
          var objectIssueResult = result.body.SuccessData
          console.log(objectIssueResult)
          this.issue = {
            issueKey: objectIssueResult.key,
            issueType: objectIssueResult.fields.issuetype.name,
            project: objectIssueResult.project
          }
          
          this.preAlertaId.setValue(objectIssueResult.fields.customfield_12501 == null ? '' : objectIssueResult.fields.customfield_12501)
          this.palletId.setValue(objectIssueResult.fields.customfield_12502  == null ? '' : objectIssueResult.fields.customfield_12502) 
          this.qntInformadaCliente.setValue(objectIssueResult.fields.customfield_16664  == null ? '' : objectIssueResult.fields.customfield_16664)
          this.customfield_12100_HP_TONER = objectIssueResult.fields.customfield_12100 == null ? 0 : objectIssueResult.fields.customfield_12100
          this.customfield_12101_NAO_HP_TONER = objectIssueResult.fields.customfield_12101 == null ? 0 : objectIssueResult.fields.customfield_12101
          this.customfield_12102_HP_TINTA = objectIssueResult.fields.customfield_12102 == null ? 0 : objectIssueResult.fields.customfield_12102
          this.customfield_12103_NAO_HP_TINTA = objectIssueResult.fields.customfield_12103 == null ? 0 : objectIssueResult.fields.customfield_12103
          
        } else {
          this.limpaInformacoesTB()
        }
      }, 
        error => console.log(error)
      )
    } else {
      this.limpaInformacoesTB()
    }
  }
  
  private updatePalletAndPrealertaField(objectIssueResult: any){
    for(var i = 0;i<objectIssueResult.fields.issuelinks.length;i++) {
      if(objectIssueResult.fields.issuelinks[i].outwardIssue === undefined){
        return;
      }
      var outwardIssue = objectIssueResult.fields.issuelinks[i].outwardIssue.key
      if(outwardIssue.startsWith('PALLET')){
        this.palletId.setValue(outwardIssue.concat('\n').concat(this.palletId.value))
      }
      if(outwardIssue.startsWith('PREALERTA')){
        this.preAlertaId.setValue(outwardIssue.concat('\n').concat(this.palletId.value))

      }
    }
  }

  private limpaInformacoesTB(){
    this.preAlertaId.setValue('')
    this.palletId.setValue('')
    this.qntInformadaCliente.setValue('')
  }

  tonerHpPlus() {
    this.customfield_12100_HP_TONER++
  }

  tonerHpMinus() {
    this.customfield_12100_HP_TONER--
  }

  tonerHPTintaPlus() {
    this.customfield_12102_HP_TINTA++
  }

  tonerHPTintaMinus() {
    this.customfield_12102_HP_TINTA--
  }

  tonerNaoHpPlus() {
    this.customfield_12101_NAO_HP_TONER++
  }

  tonerNaoHpMinus() {
    this.customfield_12101_NAO_HP_TONER--
  }

  tonerNaoHPTintaPlus() {
    this.customfield_12103_NAO_HP_TINTA++
  }

  tonerNaoHPTintaMinus() {
    this.customfield_12103_NAO_HP_TINTA--
  }

  triar() {
    if(this.issue != undefined){
      var snackBarRef = this.snackBar.open('Processo de Triagem iniciado. O Jira esta sendo atualizado. Aguarde...'
      , 'Fechar' , { duration: 3000});

      this.updateIssue();
      this.transitionIssue();
      this.limpaInformacoesTB()

      snackBarRef.afterDismissed().subscribe(() => {
        this.snackBar.open('Processo de Triagem Finalizado...', 'Fechar' , { duration: 5000});
        this.issue = undefined
      })
    }
  }
  
  private transitionIssue() {
    var jsonTransition = jsonFactory.createJsonForTransactionIssue('331').getJson()
    this.issueService.transition(this.issue.issueKey, jsonTransition).subscribe(result => {
      console.log(result)
      }, error => console.log(error)
    )
  }

  private updateIssue() {
    var jsonHpToner = jsonFactory.createJsonForUpdateFieldsNumberType("customfield_12100", this.customfield_12100_HP_TONER).getJson();
    var jsonHPTinta = jsonFactory.createJsonForUpdateFieldsNumberType("customfield_12102", this.customfield_12102_HP_TINTA).getJson();
    var jsonNaoHPToner = jsonFactory.createJsonForUpdateFieldsNumberType("customfield_12101", this.customfield_12101_NAO_HP_TONER).getJson();
    var jsonNaoHPTinta = jsonFactory.createJsonForUpdateFieldsNumberType("customfield_12103", this.customfield_12103_NAO_HP_TINTA).getJson();
    this.issueService.update(this.issue.issueKey, jsonHpToner).subscribe(result => {
      console.log(result);
    }, error => console.log(error));
    this.issueService.update(this.issue.issueKey, jsonHPTinta).subscribe(result => {
      console.log(result);
    }, error => console.log(error));
    this.issueService.update(this.issue.issueKey, jsonNaoHPToner).subscribe(result => {
      console.log(result);
    }, error => console.log(error));
    this.issueService.update(this.issue.issueKey, jsonNaoHPTinta).subscribe(result => {
      console.log(result);
    }, error => console.log(error));
  }

  isIssueExists(): boolean {
    return this.issue === undefined
  }
}