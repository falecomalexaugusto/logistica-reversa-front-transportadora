import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TriagemLexmarkComponent } from './triagem-lexmark.component';

describe('TriagemLexmarkComponent', () => {
  let component: TriagemLexmarkComponent;
  let fixture: ComponentFixture<TriagemLexmarkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TriagemLexmarkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TriagemLexmarkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
