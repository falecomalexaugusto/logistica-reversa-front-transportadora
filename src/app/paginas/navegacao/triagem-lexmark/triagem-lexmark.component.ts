import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Issue } from 'src/app/models/jira/issue';
import { IssueService } from 'src/app/servicos/jira/issue.service';
import { FormControl } from '@angular/forms';
import { REGEX_PATTERN, LONG } from 'src/app/utils/const/regex.const';
import * as jsonFactory from '../../../utils/json';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-triagem-lexmark',
  templateUrl: './triagem-lexmark.component.html',
  styleUrls: ['./triagem-lexmark.component.scss']
})
export class TriagemLexmarkComponent {

  issue: Issue
  preAlertaId = new FormControl({value: '', disabled: true})
  palletId = new FormControl({value: '', disabled: true})
  qntInformadaCliente = new FormControl({value: '', disabled: true})
  serialNumbers: Array<string> = new Array
  serialNumberForm = new FormControl({value: '', disabled: true})

  constructor(private router: Router, private issueService: IssueService,
    public snackBar: MatSnackBar) { }

  back() {
    this.router.navigate(['/sistema'])
  }

  paste(event: any){
    var value = event.clipboardData.getData('text').trim();
    if(value != '' && REGEX_PATTERN.JIRA_KEY.test(value)){
      this.issueService.findByKey(value).subscribe(result => {
        if(result.body.errorMessages === undefined){
          var objectIssueResult = result.body.SuccessData
          console.log(objectIssueResult)
          this.issue = {
            issueKey: objectIssueResult.key,
            issueType: objectIssueResult.fields.issuetype.name,
            project: objectIssueResult.project
          }
          this.preAlertaId.setValue(objectIssueResult.fields.customfield_12501 == null ? '' : objectIssueResult.fields.customfield_12501)
          this.palletId.setValue(objectIssueResult.fields.customfield_12502  == null ? '' : objectIssueResult.fields.customfield_12502) 
          this.qntInformadaCliente.setValue(objectIssueResult.fields.customfield_16664  == null ? 0 : objectIssueResult.fields.customfield_16664)

          this.serialNumberForm.enable()
        } else {
          this.limpaInformacoesTB()
        }
      }, 
        error => console.log(error)
      )
    } else {
      this.limpaInformacoesTB()
    }
  }

  private updatePalletAndPrealertaField(objectIssueResult: any){
    for(var i = 0;i<objectIssueResult.fields.issuelinks.length;i++) {
      if(objectIssueResult.fields.issuelinks[i].outwardIssue === undefined){
        return;
      }
      var outwardIssue = objectIssueResult.fields.issuelinks[i].outwardIssue.key
      if(outwardIssue.startsWith('PALLET')){
        this.palletId.setValue(outwardIssue.concat('\n').concat(this.palletId.value))
      }
      if(outwardIssue.startsWith('PREALERTA')){
        this.preAlertaId.setValue(outwardIssue.concat('\n').concat(this.palletId.value))

      }
    }
  }

  private limpaInformacoesTB(){
    this.preAlertaId.setValue('')
    this.palletId.setValue('')
    this.qntInformadaCliente.setValue('')
  }

  adicionarSerialNumber(event: any) {
    this.serialNumbers.push(event.clipboardData.getData('text').trim())
    this.serialNumberForm.setValue('')
  }

  removeSerialNumber(serialNumber: string) {
    const index: number = this.serialNumbers.indexOf(serialNumber);
    if (index !== -1) {
      this.serialNumbers.splice(index, 1);
    }   
  }

  triar() {
    if(this.issue != undefined){
      var snackBarRef = this.snackBar.open('Processo de Triagem iniciado. O Jira esta sendo atualizado. Aguarde...'
      , 'Fechar' , { duration: 3000});

      this.updateIssue();
      this.limpaInformacoesTB()
      snackBarRef.afterDismissed().subscribe(() => {
        this.snackBar.open('Processo de Triagem Finalizado...', 'Fechar' , { duration: 5000});
        this.issue = undefined
      })
    }
  }

  private updateIssue() {
    var jsonUpdateSerialNumber = jsonFactory.createJsonForUpdateTextAreaDocumentType("customfield_16594", this.serialNumbers).getJson();

    this.issueService.update(this.issue.issueKey, jsonUpdateSerialNumber).subscribe(result => {
      console.log(result)
      }, error => console.log(error)
    )
  }

  private transitionIssue() {
    var jsonTransition = jsonFactory.createJsonForTransactionIssue('331').getJson()
    this.issueService.transition(this.issue.issueKey, jsonTransition).subscribe(result => {
      console.log(result)
      }, error => console.log(error)
    )
  }

  isIssueExists(): boolean {
    return this.issue === undefined
  }
}
