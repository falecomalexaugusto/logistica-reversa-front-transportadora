import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/servicos/storage/storage.service';
import { AuthService } from 'src/app/servicos/auth/auth.service';
import { Router } from '@angular/router';
import { LocalUser } from 'src/app/models/local-user';

@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.scss']
})
export class TopMenuComponent implements OnInit {

  usuarioLogado: LocalUser;
  desabilitar: boolean;

  constructor(public storage: StorageService,
    private authService: AuthService,
    private router: Router) {

      //para teste
      this.usuarioLogado = {nome:"Jonas Gomes", email:"jonasgoms@hotmail.com", papel:"User"}
      this.storage.setLocalUser(this.usuarioLogado)
      
      if (this.storage.getLocalUser() == null) {
        alert('Voce precisa logar no sistema para acessar esses modulos.');
        this.router.navigate(['/']);
      } else {
        //this.usuarioLogado = this.storage.getLocalUser();
      }

  }

  ngOnInit() {
  }
  
  sair() {
    this.authService.logout();
  }

  voltar() {
    this.router.navigate(['/sistema'])
  }
}
