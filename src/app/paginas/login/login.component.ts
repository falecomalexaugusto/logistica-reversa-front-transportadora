import { CredencialDTO } from './../../models/credencialDTO';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialog, MatSnackBar } from '@angular/material';
import { DialogCadastroUsuarioComponent } from './dialog-cadastro-usuario/dialog-cadastro-usuario.component';
import { AuthService } from '../../servicos/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  esconderSenhaLogin = true;
  loginForm: any;

  constructor(formBuilder: FormBuilder,
              private router: Router,
              public dialog: MatDialog,
              private authService: AuthService,
              public snackBar: MatSnackBar) {

    this.loginForm = formBuilder.group({
      email: ['admin'],
      senha: ['admin']
    });

  }

  ngOnInit() {
  }

  abrirDialogCadastroUsuario(){

    this.dialog.open(DialogCadastroUsuarioComponent, {
      width: '500px',
      height: '550px'
    });

  }

  esqueciSenha() {}

  isFormularioInvalido(){
    return true
  }

  login() {
      this.router.navigate(['sistema']);
    /*
    const values = this.loginForm.value;
    const credencialDTO = new CredencialDTO(values.email, values.senha);
    this.authService.login(credencialDTO).subscribe(result => {
      if(result.length > 0){
        this.snackBar.open("Usuario logado com sucesso.", "Fechar", {
          duration: 5000
        });

        this.authService.successfullLogin(result[0].nome, result[0].email, result[0].papel);
        this.router.navigate(['sistema']);
      }else{
        this.snackBar.open("Usuario nao encontrado, valide essa informacao e tente novamente.", "Fechar", {
          duration: 5000
        });
      }
    }, error => {
      console.log('Problemas em se comunicar com o Servidor: Tente novamente');
      this.snackBar.open("Problemas em se comunicar com o Servidor: Tente novamente'.", "Fechar", {
        duration: 5000
      });
    });*/

  }

}
