import { Component,} from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { UsuarioDTO } from 'src/app/models/usuarioDTO';
import { AuthService } from '../../../servicos/auth/auth.service';
import { MatSnackBar, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-dialog-cadastro-usuario',
  templateUrl: './dialog-cadastro-usuario.component.html',
  styleUrls: ['./dialog-cadastro-usuario.component.scss']
})
export class DialogCadastroUsuarioComponent {

  esconderSenhaLogin = true;
  cadastroForm: any;

  constructor(public formBuilder: FormBuilder,
              public authService: AuthService,
              public snackBar: MatSnackBar,
              public dialogRef: MatDialogRef<DialogCadastroUsuarioComponent>) {

    this.cadastroForm = formBuilder.group({
      nome: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      senha: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  isFormularioInvalido(){
    return !this.cadastroForm.valid
  }

  cadastrarUsuario(){

    const values = this.cadastroForm.value;
    const usuario = new UsuarioDTO(values.nome, values.email, values.senha, "sem papel")
    this.authService.cadastrarUsuario(usuario).subscribe(result => {
      console.log(result)
      this.snackBar.open("Usuário Criado, agora você pode logar no sistema", "Fechar", {
        duration: 5000
      });
      this.dialogRef.close();
    });
  }

  cancelar() {
    this.dialogRef.close();
  }



}
