import { NavegacaoComponent } from './paginas/navegacao/navegacao.component';
import { LoginComponent } from './paginas/login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TriagemHpComponent } from './paginas/navegacao/triagem-hp/triagem-hp.component';
import { TriagemLexmarkComponent } from './paginas/navegacao/triagem-lexmark/triagem-lexmark.component';
import { TriagemSimpressComponent } from './paginas/navegacao/triagem-simpress/triagem-simpress.component';
import { TriagemOkidataComponent } from './paginas/navegacao/triagem-okidata/triagem-okidata.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'sistema', component: NavegacaoComponent },
  { path: 'triagem-hp', component: TriagemHpComponent },
  { path: 'triagem-lexmark', component: TriagemLexmarkComponent },
  { path: 'triagem-simpress', component: TriagemSimpressComponent },
  { path: 'triagem-okidata', component: TriagemOkidataComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
