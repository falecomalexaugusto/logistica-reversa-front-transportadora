import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './paginas/login/login.component';
import { MaterialModule } from './modulos/material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { NavegacaoComponent } from './paginas/navegacao/navegacao.component';
import { Error404Component } from './paginas/error404/error404.component';
import { DialogCadastroUsuarioComponent } from './paginas/login/dialog-cadastro-usuario/dialog-cadastro-usuario.component';
import { HttpClientModule } from '@angular/common/http';

import { TriagemHpComponent } from './paginas/navegacao/triagem-hp/triagem-hp.component';
import { TriagemSimpressComponent } from './paginas/navegacao/triagem-simpress/triagem-simpress.component';
import { TriagemOkidataComponent } from './paginas/navegacao/triagem-okidata/triagem-okidata.component';
import { TriagemLexmarkComponent } from './paginas/navegacao/triagem-lexmark/triagem-lexmark.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { TopMenuComponent } from './paginas/navegacao/top-menu/top-menu.component';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavegacaoComponent,
    Error404Component,
    DialogCadastroUsuarioComponent,
    TriagemLexmarkComponent,
    TriagemHpComponent,
    TriagemSimpressComponent,
    TriagemOkidataComponent,
    TopMenuComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatToolbarModule,
    MatInputModule,
    MatListModule
  ],
  entryComponents: [
    DialogCadastroUsuarioComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
