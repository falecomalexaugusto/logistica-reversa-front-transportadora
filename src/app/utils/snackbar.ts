import { MatSnackBar } from '@angular/material';

export class Snackbar {

    constructor(public snackBar: MatSnackBar) {}

    open(message: string, duration_time_in_ms: number) {
        this.snackBar.open(message, "Fechar", {
            duration: duration_time_in_ms
        });   
    }
}
