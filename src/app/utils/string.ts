export class StringUtils {

 constructor() {}


}

export function isNull(object: any) {
   return object === null;
}
   
export function isUndefined(object: any) {
   return object === undefined;
}
   
export function isNoContent(object: any) {
   return object === '';
}