import { EMAIL } from "./../../config/api.config";
export class JsonFactory {
  private json: string;

  constructor(json: string) {
    this.json = json;
  }

  getJson() {
    return this.json;
  }
}

export function createJsonForUpdateFieldsStringType(key: string, value: any) {
  let json = `{
                "fields": {
                  "${key}": "${value}"
                }
              }`;

  return new JsonFactory(json);
}

export function createJsonForUpdateTextAreaDocumentType(key: string, value: any) {
  let json = `{
    "fields": {
      "${key}": {
          "version": 1,
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "type": "text",
                  "text": "${value}"
                }
              ]
            }
          ]
      }
    }
  }`;

  return new JsonFactory(json);
}

export function createJsonForUpdateFieldsNumberType(key: string, value: any) {
  let json = `{
    "fields": {
      "${key}": ${value}
    }
  }`;

  return new JsonFactory(json);
}

export function createJsonForLinksCreation(
  value1: string,
  value2: string,
  value3: string
) {
  let json = `{
                "outwardIssue": { "key": "${value1}" },
                "inwardIssue": { "key": "${value2}" },
                "type": { "name": "${value3}" }
              }`;
  return new JsonFactory(json);
}

export function createJsonForComment(mensage: string) {
  let json = ` {
    "body": {
      "type": "doc",
      "version": 1,
      "content": [
        {
          "type": "paragraph",
          "content": [
            {
              "text": "${mensage}",
              "type": "text"
            }
          ]
        }
      ]
    }
  }
  `;
  return new JsonFactory(json);
}

//TODO Adicionar ENUM CUSTOM FIELD...
export function createJsonJqlSearch(jql: string) {
  let json = `{
    "expand": [
      "names",
      "schema",
      "operations"
    ],
    "jql": "${jql}",
    "maxResults": 15,
    "fieldsByKeys": true,
    "fields": [
      "summary",
      "status",
      "project",
      "issuetype",
      "customfield_16664"
    ],
    "startAt": 0
  }`;

  return new JsonFactory(json);
}

export function createJsonForCreateIssue(
  summary: string,
  issueTypeId: string,
  projectId: string
) {
  let json = ` {
    "fields": {
      "summary": "${summary}",
      "issuetype": {
        "id": "${issueTypeId}"
      },
      "project": {
        "id": "${projectId}"
      }
    }
  }
  `;

  return new JsonFactory(json);
}

export function createJsonForTransactionIssue(id: string) {
  let json = `{
    "transition": {
      "id": "${id}"
    }
  }`;

  return new JsonFactory(json);
}

export function createJsonForSendMail(html: string) {
  let json = `{
    "emails": [
      ${createArrayJsonForEmails(`${EMAIL.LOGISTICA_REVERSA}`)}
    ],
    "body": "${html}"
  }
  `;

  return new JsonFactory(json);
}

function createArrayJsonForEmails(email: string): string {
  let json = `{ "email": "${email}" }`;

  return json;
}
