//MELHORAR, isso deve virar um componente generico...

import { Issue } from 'src/app/models/jira/issue';

export class BarcodeFactory {

  private html: string;

  constructor(html: string) {
    this.html = html;
  }

  getBarcodeHTML() {
    return this.html;
  }
}


//Melhorar para criar um html generico
export function getPalletBarcode(data_hora: string, issue: Issue, element: any) {
  var html = getPalletBarcodeHtml(data_hora, issue, element);
  return new BarcodeFactory(html);
}

export function getTBLEXBarcode(volume, estado, volumes) {
  var html = getTBLEXBarcodeHtml(volume, estado, volumes);
  return new BarcodeFactory(html);
}

function getPalletBarcodeHtml(data_hora: string, issue: Issue, peso: string): string {
  var html = `
    <div id="codigo_barra" #codigo_barra style="width: 10cm; height: 10cm; display: grid; border: 1px solid black;">
      <div style="height: 2.1cm; border-bottom: 1px solid black;">
        <img style="width: 30%;" src="http://www.viabrasiltransaereo.com.br/img-1/transportadora-via-brasil-transaereo-lt-br.png" />
        <img style="width: 15%;" src="../../../../assets/images/qr_code_viabrasil.png" />
        <div style="width: 10px; margin-top: -95px; margin-left: 220px; text-aling: center;">
          <h3>Logística Reversa</h3>
          <span style="font-size: small; position: absolute; margin-top: -15px;">São Paulo - (11) 2085-4400 | </span>
          <br/>
          <span style="font-size: small; position: absolute; margin-top: -18px;">www.viabrasil.com.br</span>
        </div>
      </div>

      <div class="row" >
          <div>
              <h5 style="font-size: 15px;">PALLET ID</h5>
          </div>

          <div style="margin-left: 100px; margin-top: -50px;">
            ${document.getElementsByClassName('barcode')[0].innerHTML}
          </div>
          <h4 style="transform: rotate(-90deg); margin-left: 265px; margin-top: -75px;">${data_hora}</h4>
      </div>

      <h2 style="margin-left: 105px; margin-top: -50px">${peso}</h4>
    </div>`;

  return html;
}

function getTBLEXBarcodeHtml(volume, estado, volumes): string {
  var html = `
    <div id="codigo_barra" #codigo_barra style="width: 10cm; height: 10cm; display: grid; border: 1px solid black;">

      <div style="height: 2cm;">
          <img style="width: 45%;" src="http://www.wertlog.com.br/img/[transparente]%20logo_wert_rgb_reduzido.png" />
          <img style="width: 45%;" src="http://www.sinctronics.com.br/img/logo.png" />
      </div>

      <div class="row" style="margin-top: -5px;">

          <div class="column" style="margin-top: 5px; margin-left: -10px;">
              <h4 style="font-size: 15px;">CHAMADO</h4>
          </div>

          <div class="column" style="margin-left: -15px;">
            ${document.getElementsByClassName('barcode')[0].innerHTML}
          </div>

      </div>

      <div class="row" style="margin-top: -5px;">

          <div class="column" style="margin-top: 5px; margin-left: 5px;">
              <h4 style="font-size: 15px;">CARTA</h4>
          </div>

          <div class="column" style="margin-left: 0px">
            ${document.getElementsByClassName('barcode')[1].innerHTML}
          </div>
      </div>
      <div id="origem" style="border-top: 1px solid black; width: 103px;
              height: 100px; margin: 0px 10px 0px 273px;
              border-left: 1px solid black;">
          <div style="margin: 0px 0px 0px 0px; border-bottom: 1px solid black;">
              ORIGEM: <b>${estado}</b>
          </div>
          <br />
          <mat-divider></mat-divider>
          <div style="margin: 0px 0px 0px 15px;">
              VOLUMES:
          </div>
          <br />
          <div style="margin-top: -45px; margin-left: 20px;">
              <b>
                  <h2>${volume} / ${volumes}</h2>
              </b>
          </div>
      </div>
  </div>
  `;

  return html;
}


