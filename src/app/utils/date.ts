
export class DateFactory {

  private data: string;
  private data_hora: string;
  private hora: string;

  constructor(data: string, data_hora: string, hora: string) {
    this.data = data;
    this.data_hora = data_hora;
    this.hora = hora;
  }

  getData() {
    return this.data;
  }

  getDataHora(){
    return this.data_hora;
  }

  getHora(){
    return this.hora;
  }
}


export function crateDate(data: Date) {
  const _data = formatarDataDD_MM_YYYY(data);
  const _data_hora = formatarDataDD_MM_YYYY_HH_MM(data);
  const _hora = formatarHora_HH_MM_SS(data);

  return new DateFactory(_data, _data_hora, _hora);
}

function formatarDataDD_MM_YYYY_HH_MM(data: Date) {
  let dia  = data.getDate().toString()
  let diaF = (dia.length == 1) ? '0' + dia : dia
  let mes  = (data.getMonth() + 1).toString() //+1 pois no getMonth Janeiro começa com zero.
  let mesF = (mes.length == 1) ? '0' + mes : mes
  let anoF = data.getFullYear();

  let hora = formatarHora_HH_MM_SS(data);

  return diaF + '/' + mesF + '/' + anoF + ' - ' + hora;
}

function formatarDataDD_MM_YYYY(data: Date) {
  let dia  = data.getDate().toString();
  let diaF = (dia.length == 1) ? '0' + dia : dia;
  let mes  = (data.getMonth() + 1).toString(); //+1 pois no getMonth Janeiro começa com zero.
  let mesF = (mes.length == 1) ? '0' + mes : mes;
  let anoF = data.getFullYear();

  return diaF + '/' + mesF + '/' + anoF;
}

function formatarHora_HH_MM_SS(data: Date) {
  let dia  = data.getDate().toString();
  let mes  = (data.getMonth() + 1).toString(); //+1 pois no getMonth Janeiro começa com zero.

  return data.getHours() + ':' + data.getMinutes() + ":" + data.getSeconds();
}
