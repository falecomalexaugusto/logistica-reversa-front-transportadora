import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatIconModule, MatInputModule,
  MatListModule, MatMenuModule, MatSidenavModule,
  MatToolbarModule, MatCardModule, MatDialogModule,
  MatDividerModule, MatTooltipModule, MatTabsModule, MatRadioModule, MatGridListModule, MatSnackBarModule, MatTableModule, MatSelectModule, MatCheckboxModule, MatDatepickerModule, MatNativeDateModule, MatPaginatorModule, MatProgressSpinnerModule
} from '@angular/material';


@NgModule({
  declarations: [],
  imports: [
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatCardModule,
    MatDialogModule,
    MatDividerModule,
    MatTooltipModule,
    MatDialogModule,
    MatTabsModule,
    MatRadioModule,
    MatSidenavModule,
    MatGridListModule,
    MatSnackBarModule,
    MatTableModule,
    MatSelectModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatPaginatorModule,
    MatNativeDateModule,
    MatProgressSpinnerModule
  ],
  exports: [
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatCardModule,
    MatDialogModule,
    MatDividerModule,
    MatTooltipModule,
    MatDialogModule,
    MatTabsModule,
    MatRadioModule,
    MatSidenavModule,
    MatGridListModule,
    MatSnackBarModule,
    MatTableModule,
    MatSelectModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatPaginatorModule,
    MatNativeDateModule,
    MatProgressSpinnerModule
  ],
  providers: [
    MatDatepickerModule,
  ]
})
export class MaterialModule { }

