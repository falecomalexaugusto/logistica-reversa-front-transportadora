# logistica-reversa-front-transportadora

# Passos necessários para subir a aplicação: Instalação dos pacotes...

* Instalação do Node e NPM
* https://nodejs.org/en/

* Entrar no diretório raiz do projeto e executar o comando: 

`npm install`

`ng serve -o`

* Caso falhar então instale o angular cli
` npm install -g @angular/cli `

* CAso o comando ng não for reconhecido, é necessario adicionar o angular como variavel de ambiente